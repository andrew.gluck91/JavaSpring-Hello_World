#TO BUILD THE APPLICATION:

    #Install prerequisite software
        - Docker
            - For windows you can download and install the windows version this works out of the box
            - For Any major linux distro please run the install_docker_linux.sh file included with this project

    #From the project directory you can run either build.bat or build.sh depending on your OS
        This will
            - automatically build the java application to build/libs/<name>.jar
            - Build a base java docker container
            - Build up required libraries
            - Push the built jar to the container
            - Run the docker container, detached, binding the service to port 8080

    You should now have a working REST java application
    You may see the initial out put in your browser.
    Navigate to: localhost:8080/greeting
    If you encounter any trouble in this build please open an issue request.