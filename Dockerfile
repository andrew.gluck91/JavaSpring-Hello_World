FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD "build/libs/gs-rest-service-0.1.0.jar" myapp.jar
RUN sh -c 'touch /myapp.jar'
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/myapp.jar"]